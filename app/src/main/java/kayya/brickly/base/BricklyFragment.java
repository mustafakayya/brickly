package kayya.brickly.base;


import android.app.Activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import kayya.brickly.MainActivity;
import kayya.brickly.models.AdapterStates;

/**
 * Created by mustafakaya on 04/08/16.
 */
public abstract class BricklyFragment extends Fragment {

    public View getRootView() {
        return getBricklyActivity().getRootView();
    }

    public void changePagerState(AdapterStates state){
        Activity activity = getActivity();
        if (activity != null && activity instanceof MainActivity) {
            ((MainActivity) activity).setStates(state);
        }
    }


    public void trackButtonClickEvent(String action){
        if (getBricklyActivity()!=null)
            getBricklyActivity().trackButtonClickEvent(action);
    }

    public BricklyActivity getBricklyActivity(){
        return (BricklyActivity) getActivity();
    }

    public void hideKeyboard(){
        if(getView()!=null) {
            View view = getView().findFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getBricklyActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

}
