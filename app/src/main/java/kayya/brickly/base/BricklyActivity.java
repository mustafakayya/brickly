package kayya.brickly.base;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.analytics.HitBuilders;

import kayya.brickly.BricklyApplication;

/**
 * Created by mustafakaya on 04/08/16.
 */
public abstract class BricklyActivity extends AppCompatActivity{

    public abstract View getRootView();


    private BricklyApplication getBricklyApplication(){
        return (BricklyApplication) getApplication();
    }

    public void trackButtonClickEvent(String action){
        getBricklyApplication().getDefaultTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Button Click")
                .setAction(action)
                .build());
    }

    public void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
