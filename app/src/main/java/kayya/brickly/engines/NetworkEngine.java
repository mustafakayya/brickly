package kayya.brickly.engines;

import kayya.brickly.network.BricklyApi;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class NetworkEngine {

    BricklyApi bricklyApi;

    private static NetworkEngine ourInstance = new NetworkEngine();

    public static NetworkEngine getInstance() {
        return ourInstance;
    }

    private NetworkEngine() {
        initializeApiService();
    }

    private void initializeApiService(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addNetworkInterceptor(loggingInterceptor).build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://178.62.214.69").client(client).addConverterFactory(GsonConverterFactory.create()).build();
        bricklyApi = retrofit.create(BricklyApi.class);
    }

    public BricklyApi getBricklyApi() {
        if(bricklyApi==null)
            initializeApiService();
        return bricklyApi;
    }

}
