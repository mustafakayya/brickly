package kayya.brickly.engines;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mustafakaya on 04/08/16.
 */
public class SharedPreferencesEngine {

    private static SharedPreferencesEngine ourInstance ;

    public static SharedPreferencesEngine getInstance(Context context) {
        if(ourInstance == null || ourInstance.preferences == null)
            ourInstance = new SharedPreferencesEngine(context);

        return ourInstance;
    }

    private static final String LOGIN_KEY="login";
    SharedPreferences preferences;

    private SharedPreferencesEngine(Context context) {
        preferences = context.getSharedPreferences("BricklyPrefs",Context.MODE_PRIVATE);
    }

    public void userLogin(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(LOGIN_KEY,true);
        editor.commit();
    }

    public void userLogout(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(LOGIN_KEY,false);
        editor.commit();
    }

    public boolean isUserLogin(){
        return preferences.getBoolean(LOGIN_KEY,false);
    }
}
