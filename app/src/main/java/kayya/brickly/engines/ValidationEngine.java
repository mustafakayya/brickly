package kayya.brickly.engines;

/**
 * Created by mustafakaya on 04/08/16.
 */
public class ValidationEngine {

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isPasswordsMatch(String password,String passwordConfirmation){
        return password.equals(passwordConfirmation);
    }

    public static boolean isValidPassword(String password){
        return password.length()>3;
    }

    public static boolean isValidName(String name){
        return name.length()>1;
    }
}
