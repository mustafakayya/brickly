package kayya.brickly.models;

/**
 * Created by mustafakaya on 04/08/16.
 */
public enum AdapterStates {
    None,ForgetPassword,Profile;


    public static AdapterStates fromOrdinal(int ordinal){
        switch (ordinal){
            case 0:
                return None;

            case 1:
                return ForgetPassword;

            case 2:
                return Profile;

            default:
                return None;
        }
    }
}
