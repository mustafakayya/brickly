package kayya.brickly.models;

import android.support.v4.app.Fragment;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class AdapterItem {

    Fragment fragment;
    String title;

    public AdapterItem(Fragment fragment, String title) {
        this.fragment = fragment;
        this.title = title;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
