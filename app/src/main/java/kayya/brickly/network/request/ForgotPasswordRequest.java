package kayya.brickly.network.request;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class ForgotPasswordRequest {

    String email;

    public ForgotPasswordRequest(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
