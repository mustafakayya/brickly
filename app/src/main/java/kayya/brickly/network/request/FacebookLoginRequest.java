package kayya.brickly.network.request;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class FacebookLoginRequest {

    String accessToken;

    public FacebookLoginRequest(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
