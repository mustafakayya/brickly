package kayya.brickly.network.request;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class LogoutRequest {

    String email;

    public LogoutRequest(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
