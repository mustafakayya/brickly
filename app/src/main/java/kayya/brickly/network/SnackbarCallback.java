package kayya.brickly.network;

/**
 * Created by mustafakaya on 04/08/16.
 */

import android.support.design.widget.Snackbar;
import android.view.View;

import retrofit2.Call;

public abstract class SnackbarCallback<T> extends Callback<T> {
    private View view;

    public SnackbarCallback(View view) {
        this.view = view;
    }

    public SnackbarCallback(Callback<T> chain, View view) {
        super(chain);
        this.view = view;
    }

    public View getView() {
        return view;
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
         getSnackbar(view,t.getMessage()).show();
    }


    public  Snackbar getSnackbar(View view, String text) {
        return Snackbar.make(view, text, Snackbar.LENGTH_LONG);
    }
}
