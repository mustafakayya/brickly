package kayya.brickly.network;

import android.app.ProgressDialog;
import android.view.View;

import retrofit2.Call;

/**
 * Created by mustafakaya on 04/08/16.
 */
public abstract class ProgressCallback<T> extends SnackbarCallback<T> {
    private ProgressDialog dialog;

    public ProgressCallback(View view) {
        super(view);
        initDialog();
    }

    public ProgressCallback(Callback<T> chain, View view) {
        super(chain, view);
        initDialog();
    }

    private void initDialog() {
        if ( getView() != null && getView().getContext() != null)
        dialog = ProgressDialog.show(getView().getContext(), "", "", true);
    }

    @Override
    protected void handleChain(T response) {
        super.handleChain(response);
        dialog.dismiss();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        super.onFailure(call, t);
        dialog.dismiss();
    }
}
