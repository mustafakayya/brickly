package kayya.brickly.network;

import kayya.brickly.network.request.FacebookLoginRequest;
import kayya.brickly.network.request.ForgotPasswordRequest;
import kayya.brickly.network.request.LoginRequest;
import kayya.brickly.network.request.LogoutRequest;
import kayya.brickly.network.request.SignupRequest;
import kayya.brickly.network.response.FacebookLoginResponse;
import kayya.brickly.network.response.ForgotPasswordResponse;
import kayya.brickly.network.response.HomeScreenHeaderResponse;
import kayya.brickly.network.response.LoginResponse;
import kayya.brickly.network.response.LogoutResponse;
import kayya.brickly.network.response.SignupResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by mustafakaya on 03/08/16.
 */
public interface BricklyApi {

    @POST("/index.php?method=reset")
    Call<ForgotPasswordResponse> forgotPassword(@Body ForgotPasswordRequest request);

    @POST("/index.php?method=login")
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST("/index.php?method=logout")
    Call<LogoutResponse> logout(@Body LogoutRequest request);

    @POST("/index.php?method=register")
    Call<SignupResponse> signup(@Body SignupRequest request);

    @POST("/index.php?method=fb")
    Call<FacebookLoginResponse> facebookLogin(@Body FacebookLoginRequest request);

    @GET("/index.php?method=home")
    Call<HomeScreenHeaderResponse> getHomeScreenHeader();


}
