package kayya.brickly.network.response;

/**
 * Created by mustafakaya on 03/08/16.
 */
public abstract class BricklyBaseResponse {

    int success;

    public BricklyBaseResponse() {
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
