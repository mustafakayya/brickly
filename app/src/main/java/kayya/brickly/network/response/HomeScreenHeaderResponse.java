package kayya.brickly.network.response;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class HomeScreenHeaderResponse extends BricklyBaseResponse{

    String title;

    public HomeScreenHeaderResponse() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
