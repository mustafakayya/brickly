package kayya.brickly.network;


import kayya.brickly.network.response.BricklyBaseResponse;
import retrofit2.Call;


/**
 * Created by mustafakaya on 04/08/16.
 */
public abstract class Callback<T> implements retrofit2.Callback<T> {
    private Callback<T> chain;

    public Callback() {
        this(null);
    }

    public Callback(Callback<T> chain) {
        this.chain = chain;
    }

    public abstract void onResponse(T response);

    protected void handleChain(T response) {
        onResponse(response);
        if (chain != null)
            chain.handleChain(response);
    }


    @Override
    public void onResponse(Call<T> call, retrofit2.Response<T> response) {
        try {
            T body = response.isSuccessful() ? response.body() : null;
            if (response==null)
                throw new RuntimeException("Network Error");
            if (((BricklyBaseResponse)body).getSuccess() != 1)
                throw new RuntimeException("Error Code 34546");
            handleChain(body);
        } catch (Exception e) {
            onFailure(call, e);
        }
    }
}