package kayya.brickly.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import kayya.brickly.R;
import kayya.brickly.base.BricklyFragment;
import kayya.brickly.engines.NetworkEngine;
import kayya.brickly.engines.SharedPreferencesEngine;
import kayya.brickly.engines.ValidationEngine;
import kayya.brickly.models.AdapterStates;
import kayya.brickly.network.ProgressCallback;
import kayya.brickly.network.request.SignupRequest;
import kayya.brickly.network.response.SignupResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class SignupFragment extends BricklyFragment implements View.OnClickListener{

    AppCompatEditText edtName;
    AppCompatEditText edtPassword;
    AppCompatEditText edtPasswordConfirm;
    AppCompatEditText edtMail;
    AppCompatButton btnConfirm;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_signup,container,false);

        edtName = (AppCompatEditText)mainView.findViewById(R.id.edtName_fSignup);
        edtMail = (AppCompatEditText)mainView.findViewById(R.id.edtMail_fSignup);
        edtPassword = (AppCompatEditText)mainView.findViewById(R.id.edtPassword_fSignup);
        edtPasswordConfirm = (AppCompatEditText)mainView.findViewById(R.id.edtPasswordConfirm_fSignup);
        btnConfirm = (AppCompatButton)mainView.findViewById(R.id.btnConfirm_fSignup);
        btnConfirm.setOnClickListener(this);

        return mainView;
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        if(v == btnConfirm){
            trackButtonClickEvent("Sign Up");
            sendRegisterRequest();
        }
    }

    private void sendRegisterRequest() {
        String name = edtName.getText().toString();
        String mail = edtMail.getText().toString();
        String password = edtPassword.getText().toString();
        String passwordConfirmation = edtPasswordConfirm.getText().toString();

        edtName.setError(null);
        edtMail.setError(null);
        edtPassword.setError(null);
        edtPasswordConfirm.setError(null);



        if (!ValidationEngine.isValidName(name)){
            edtName.setError(getString(R.string.invalid_name));
            return;
        }

        if (!ValidationEngine.isValidEmail(mail)){
            edtMail.setError(getString(R.string.invalid_email));
            return;
        }

        if ( !ValidationEngine.isValidPassword(password)){
            edtPassword.setError(getString(R.string.invalid_password));
            return;
        }

        if ( !ValidationEngine.isPasswordsMatch(password,passwordConfirmation)){
            edtPasswordConfirm.setError(getString(R.string.passwords_doesnt_match));
            return;
        }

        SignupRequest request = new SignupRequest(name,mail,password);
        NetworkEngine.getInstance().getBricklyApi().signup(request).enqueue(new ProgressCallback<SignupResponse>(getRootView()) {
            @Override
            public void onResponse(SignupResponse response) {
                SharedPreferencesEngine.getInstance(getActivity()).userLogin();
                changePagerState(AdapterStates.Profile);
            }
        });

    }
}
