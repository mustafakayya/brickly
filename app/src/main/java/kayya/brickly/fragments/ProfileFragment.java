package kayya.brickly.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.login.LoginManager;


import kayya.brickly.R;
import kayya.brickly.base.BricklyFragment;
import kayya.brickly.engines.NetworkEngine;
import kayya.brickly.engines.SharedPreferencesEngine;
import kayya.brickly.models.AdapterStates;
import kayya.brickly.network.ProgressCallback;
import kayya.brickly.network.request.LogoutRequest;
import kayya.brickly.network.response.LogoutResponse;


/**
 * Created by mustafakaya on 03/08/16.
 */
public class ProfileFragment extends BricklyFragment implements View.OnClickListener {

    AppCompatButton btnLogout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_profile, container, false);
        btnLogout = (AppCompatButton) mainView.findViewById(R.id.btnLogout_fLogout);
        btnLogout.setOnClickListener(this);
        return mainView;
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        if ( v == btnLogout ){
            trackButtonClickEvent("Logout");
            showConfirmationDialog();
        }
    }

    private void showConfirmationDialog() {
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).setMessage(getString(R.string.logout_message)).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendLogoutRequest();
                dialog.dismiss();
            }


        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
        dialog.show();
    }

    private void sendLogoutRequest() {
        LogoutRequest request = new LogoutRequest("test@brickly.io");
        NetworkEngine.getInstance().getBricklyApi().logout(request).enqueue(new ProgressCallback<LogoutResponse>(getRootView()) {
            @Override
            public void onResponse(LogoutResponse response) {
                onUserLogout();
            }
        });
    }

    private void onUserLogout() {
        LoginManager.getInstance().logOut();
        SharedPreferencesEngine.getInstance(getActivity()).userLogout();
        changePagerState(AdapterStates.None);
    }


}
