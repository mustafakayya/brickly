package kayya.brickly.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import kayya.brickly.R;
import kayya.brickly.base.BricklyFragment;
import kayya.brickly.engines.NetworkEngine;
import kayya.brickly.engines.ValidationEngine;
import kayya.brickly.models.AdapterStates;
import kayya.brickly.network.ProgressCallback;
import kayya.brickly.network.request.ForgotPasswordRequest;
import kayya.brickly.network.response.ForgotPasswordResponse;
import kayya.brickly.network.response.LogoutResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class ForgotPasswordFragment extends BricklyFragment implements View.OnClickListener{



    AppCompatEditText edtMail;
    AppCompatButton btnConfirm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_forgotpassword,container,false);
        edtMail = (AppCompatEditText)mainView.findViewById(R.id.edtMail_fForgotPassword);
        btnConfirm = (AppCompatButton) mainView.findViewById(R.id.btnConfirm_fForgotPassword);
        btnConfirm.setOnClickListener(this);
        return mainView;
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        if ( v == btnConfirm){
            trackButtonClickEvent("ForgetPassword");
            edtMail.setError(null);
            String mail = edtMail.getText().toString();
            if (ValidationEngine.isValidEmail(mail))
                sendForgotPasswordRequest(mail);
            else
                edtMail.setError(getString(R.string.invalid_email));
        }


    }

    private void sendForgotPasswordRequest(String mail) {
        ForgotPasswordRequest request = new ForgotPasswordRequest(mail);
        NetworkEngine.getInstance().getBricklyApi().forgotPassword(request).enqueue(new ProgressCallback<ForgotPasswordResponse>(getRootView()) {
            @Override
            public void onResponse(ForgotPasswordResponse response) {
                showInfoDialog();
            }
        });
    }

    private void showInfoDialog() {
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.success)).setMessage(getString(R.string.mailbox_message))
                .setCancelable(false).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                changePagerState(AdapterStates.None);
            }
        }).create();
        dialog.show();
    }


}
