package kayya.brickly.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import kayya.brickly.MainActivity;
import kayya.brickly.R;
import kayya.brickly.base.BricklyFragment;
import kayya.brickly.engines.NetworkEngine;
import kayya.brickly.engines.SharedPreferencesEngine;
import kayya.brickly.engines.ValidationEngine;
import kayya.brickly.models.AdapterStates;
import kayya.brickly.network.ProgressCallback;
import kayya.brickly.network.request.LoginRequest;
import kayya.brickly.network.response.LoginResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class LoginFragment extends BricklyFragment implements View.OnClickListener {



    AppCompatEditText edtMail;
    AppCompatEditText edtPassword;
    AppCompatButton btnConfirm;
    TextView lblForgetPassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_login, container, false);
        edtMail = (AppCompatEditText) mainView.findViewById(R.id.edtMail_fLogin);
        edtPassword = (AppCompatEditText) mainView.findViewById(R.id.edtPassword_fLogin);
        btnConfirm = (AppCompatButton) mainView.findViewById(R.id.btnConfirm_fLogin);
        lblForgetPassword = (TextView) mainView.findViewById(R.id.lblForget_fLogin);
        btnConfirm.setOnClickListener(this);
        lblForgetPassword.setOnClickListener(this);
        return mainView;
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        if (v == btnConfirm) {
            sendLoginRequest();
            trackButtonClickEvent("Login");
        } else if (v == lblForgetPassword) {
            trackButtonClickEvent("OpenForgetPassword");
            openForgetPasswordFragment();
        }
    }

    private void openForgetPasswordFragment() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof MainActivity) {
            ((MainActivity) activity).setStates(AdapterStates.ForgetPassword);
        }
    }

    private void sendLoginRequest() {
        String mail = edtMail.getText().toString();
        String password = edtPassword.getText().toString();
        edtMail.setError(null);
        edtPassword.setError(null);
        if (!ValidationEngine.isValidEmail(mail)){
            edtMail.setError(getString(R.string.invalid_email));
            return;
        }

        if ( !ValidationEngine.isValidPassword(password)){
            edtPassword.setError(getString(R.string.invalid_password));
            return;
        }


        LoginRequest request = new LoginRequest(mail, password);
        NetworkEngine.getInstance().getBricklyApi().login(request).enqueue(new ProgressCallback<LoginResponse>(getRootView()) {
            @Override
            public void onResponse(LoginResponse response) {
                onUserLogin();
            }
        });


    }

    private void onUserLogin() {

        SharedPreferencesEngine.getInstance(getActivity()).userLogin();
        changePagerState(AdapterStates.Profile);
    }

}
