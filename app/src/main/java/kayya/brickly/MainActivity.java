package kayya.brickly;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import kayya.brickly.adapters.BricklyPagerAdapter;
import kayya.brickly.base.BricklyActivity;
import kayya.brickly.engines.NetworkEngine;
import kayya.brickly.engines.SharedPreferencesEngine;
import kayya.brickly.fragments.ForgotPasswordFragment;
import kayya.brickly.fragments.LoginFragment;
import kayya.brickly.fragments.ProfileFragment;
import kayya.brickly.fragments.SignupFragment;
import kayya.brickly.models.AdapterItem;
import kayya.brickly.models.AdapterStates;
import kayya.brickly.network.ProgressCallback;
import kayya.brickly.network.request.FacebookLoginRequest;
import kayya.brickly.network.response.FacebookLoginResponse;
import kayya.brickly.network.response.HomeScreenHeaderResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BricklyActivity implements View.OnClickListener{



    CoordinatorLayout coordinatorLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    AdapterStates states;
    BricklyPagerAdapter bricklyPagerAdapter;
    AppCompatButton btnFacebook;
    CallbackManager callbackManager;
    TextView lblHeader;

    private static final String STATE_KEY="state";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.rootCordinator_aMain);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsinToolbar_aMain);
        toolbar = (Toolbar)findViewById(R.id.toolbar_aMain);
        viewPager = (ViewPager) findViewById(R.id.viewPager_aMain);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout_aMain);
        btnFacebook = (AppCompatButton)findViewById(R.id.btnFacebook_aMain);
        lblHeader = (TextView)findViewById(R.id.lblHeader_aMain);

        btnFacebook.setOnClickListener(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        initPager();
        initFacebook();
        fetchHeaderText();

        if(savedInstanceState == null){
            if(SharedPreferencesEngine.getInstance(this).isUserLogin())
                setStates(AdapterStates.Profile);
            else
                setStates(AdapterStates.None);
        }else{
            setStates(AdapterStates.fromOrdinal(savedInstanceState.getInt(STATE_KEY,0)));
        }


    }

    private void fetchHeaderText() {
        NetworkEngine.getInstance().getBricklyApi().getHomeScreenHeader().enqueue(new Callback<HomeScreenHeaderResponse>() {
            @Override
            public void onResponse(Call<HomeScreenHeaderResponse> call, Response<HomeScreenHeaderResponse> response) {
                if ( response.isSuccessful() && response.body()!=null && response.body().getSuccess()==0){
                    lblHeader.setText(response.body().getTitle() == null ? "Brickly" : response.body().getTitle());
                }else{
                    lblHeader.setText("Brickly");
                }
            }

            @Override
            public void onFailure(Call<HomeScreenHeaderResponse> call, Throwable t) {
                lblHeader.setText("Brickly");
            }
        });
    }


    public void setStates(AdapterStates states) {
        this.states = states;
        List<AdapterItem> adapterItems = new ArrayList<>();
        switch (states){
            case None:
                btnFacebook.setVisibility(View.VISIBLE);
                AdapterItem loginItem = new AdapterItem(new LoginFragment(),getString(R.string.login_title));
                AdapterItem signupItem = new AdapterItem(new SignupFragment(),getString(R.string.signup_title));
                adapterItems.add(loginItem);
                adapterItems.add(signupItem);
                break;
            case ForgetPassword:
                btnFacebook.setVisibility(View.VISIBLE);
                AdapterItem forgetPasswordItem = new AdapterItem(new ForgotPasswordFragment(),getString(R.string.forget_password_title));
                adapterItems.add(forgetPasswordItem);
                break;
            case Profile:
                btnFacebook.setVisibility(View.GONE);
                AdapterItem profileItem = new AdapterItem(new ProfileFragment(),getString(R.string.profile_title));
                adapterItems.add(profileItem);
                break;
        }

        bricklyPagerAdapter.setItems(adapterItems);
        bricklyPagerAdapter.notifyDataSetChanged();
    }

    private void initPager() {
        bricklyPagerAdapter = new BricklyPagerAdapter(getSupportFragmentManager(),new ArrayList<AdapterItem>());
        viewPager.setAdapter(bricklyPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }


    private void initFacebook() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        sendFacebookLoginRequest(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Snackbar.make(coordinatorLayout,getString(R.string.facebook_cancelled),Snackbar.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Snackbar.make(coordinatorLayout,exception.toString(),Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    private void sendFacebookLoginRequest(AccessToken accessToken) {
        FacebookLoginRequest request = new FacebookLoginRequest(accessToken.getToken());
        NetworkEngine.getInstance().getBricklyApi().facebookLogin(request).enqueue(new ProgressCallback<FacebookLoginResponse>(coordinatorLayout) {
            @Override
            public void onResponse(FacebookLoginResponse response) {
                SharedPreferencesEngine.getInstance(MainActivity.this).userLogin();
                setStates(AdapterStates.Profile);
            }
        });
    }


    @Override
    public void onClick(View v) {
        hideKeyboard();
        if(v == btnFacebook){
            trackButtonClickEvent("Facebook Login");
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onBackPressed() {
        if(states!=AdapterStates.ForgetPassword)
            super.onBackPressed();
        else
            setStates(AdapterStates.None);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_KEY,states.ordinal());

    }


    @Override
    public View getRootView() {
        return coordinatorLayout;
    }
}
