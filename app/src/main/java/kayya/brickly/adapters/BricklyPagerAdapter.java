package kayya.brickly.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

import kayya.brickly.models.AdapterItem;

/**
 * Created by mustafakaya on 03/08/16.
 */
public class BricklyPagerAdapter extends FragmentStatePagerAdapter{

   List<AdapterItem> items;

    public BricklyPagerAdapter(FragmentManager fm, List<AdapterItem> items) {
        super(fm);
        if(items == null)
            items = new ArrayList<>();
        this.items = items;
    }

    public void setItems(List<AdapterItem> items) {
        if(items == null)
            items = new ArrayList<>();
        this.items = items;
    }

    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        return items.get(position).getFragment();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return items.get(position).getTitle();
    }

    @Override
    public int getCount() {
        return items.size();
    }
}
