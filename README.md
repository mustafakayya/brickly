# Brickly Android Task #



This project develop by Mustafa Kaya for Brickly.

Basically, implementing of simple auth layer.


### Design ###

http://www.materialup.com/posts/login-and-signup-mobile. 

### Requirements (Must) ###

- Pass all form fields as a post parameter. 
- Email must validated. Otherwise, user must see an error message as an inline error. 
- If success field is 0 in api response, you must fire error message like “Error code 34175” as a modal box.
- Sessions must work 
- After logout, user must redirect to login/register view. 
- When user clicks on logout, we need to see confirm box like “Are you sure?”
- When user clicks on fb login, you should use fb sdk to obtain fb token. And pass this token to http://178.62.214.69/fb.json as a post parameter.
- After login, use must redirect to user profile page which only says “After login Page"
- Support multiple screen (tablet and phone)

### Requirements (Nice to have) ###

- Effects 
- Analytics integration 

### End Points ###

- Login : http://178.62.214.69/login.json
- Logout : http://178.62.214.69/logout.json
- Register : http://178.62.214.69/register.json
- Forgot Password : http://178.62.214.69/reset.json
- Facebook Login : http://178.62.214.69/fb.json 
- Home Screen Header text : http://178.62.214.69/home.json 

### What I used ###


- Android Design Library
- Fragments
- Retrofit
- OKHTTP
- Facebook Android SDK
- Crashlytics
- Google Analytcs










